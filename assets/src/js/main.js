// Затемение экрана//
const overlay = document.querySelector('.overlay')

// Бургер меню //
const burgerButton = document.querySelector(".mobile-menu__button");
const mobileMenu = document.querySelector(".mobile-menu__nav");

burgerButton.addEventListener('click', () => {
	mobileMenu.classList.toggle('mobile-menu__nav--active');
	overlay.classList.toggle('overlay--active');
})

// Навигация Табы //
const navMenu = document.querySelector(".navigation__wrapper");
const navItem = document.querySelectorAll(".nav-menu__item");
const dropdownList = document.querySelectorAll(".dropdown__list");

navItem.forEach(function (element) {
	element.addEventListener('click', function (event) {
		const $btn = event.target.closest('.nav-menu__item');
		const id = $btn.dataset.id

		navItem.forEach(btn => {
      		btn.classList.remove("nav--active");
    	});
    	
		$btn.classList.add("nav--active");

    	dropdownList.forEach(content => {
      		content.classList.remove("nav--active");
		});

    	const element = document.getElementById(id);
    	element.classList.add("nav--active");
	})
})


// Валидация формы //
;(function() {
	'use strict';

	class Form {
		
		static patternName = /^[а-яёА-ЯЁ\s]+$/;
    	static patternPhone = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
		static errorMess = [
			'Введите имя', // 0
			'Неверное имя', // 1
      		'Введите телефон', // 2
      		'Неверный телефон', // 3
		];

		constructor(form) {
			this.form = form;
			this.fields = this.form.querySelectorAll('.form-control');
			this.btn = this.form.querySelector('[type=submit]');
			this.iserror = false;
			this.registerEventsHandler();
		}

		static getElement(el) {
			return el.parentElement.nextElementSibling;
		}

		registerEventsHandler() {
			this.btn.addEventListener('click', this.validForm.bind(this));
			this.form.addEventListener('focus', () => {
				const el = document.activeElement;
				if (el === this.btn) return;
				this.cleanError(el);
			}, true);
			for (let field of this.fields) {
				field.addEventListener('blur', this.validBlurField.bind(this));
			}
		}

		validForm(e) {
			e.preventDefault();
			const formData = new FormData(this.form);
			let error;

			for (let property of formData.keys()) {
				error = this.getError(formData, property);
				if (error.length == 0) continue;
				this.iserror = true;
				this.showError(property, error);
			}

			if (this.iserror) return;
			this.sendFormData(formData);
		}

		validBlurField(e) {
			const target = e.target;
			const property = target.getAttribute('name');
			const value = target.value;
			const formData = new FormData();
			formData.append(property, value);
			const error = this.getError(formData, property);
			if (error.length == 0) return;
			this.showError(property, error);
		}

		getError(formData, property) {
			let error = '';
			const validate = {
				username: () => {
          			if (formData.get('username').length == 0) {
						error = Form.errorMess[0];
					} else if (Form.patternName.test(formData.get('username')) == false) {
						error = Form.errorMess[1];
					}
				},
				
				userphone: () => {
					if (formData.get('userphone').length == 0) {
						error = Form.errorMess[2];
					} else if (Form.patternPhone.test(formData.get('userphone')) == false) {
						error = Form.errorMess[3];
					}
				},
			}

			if (property in validate) {
				validate[property]();
			}
			return error;
		}

		showError(property, error) {
			const el = this.form.querySelector(`[name=${property}]`);
			const errorBox = Form.getElement(el);
			el.classList.add('form-control_error');
			errorBox.innerHTML = error;
			errorBox.style.display = 'block';
		}

		cleanError(el) {
			const errorBox = Form.getElement(el);
			el.classList.remove('form-control_error');
			errorBox.removeAttribute('style');
			this.iserror = false;
		}

		sendFormData(formData) {
			let xhr = new XMLHttpRequest();
			const form = document.querySelector('.form')
            const formButton = document.querySelector('.form__button')
            const formSent = document.querySelector('.sent__form')
			
			xhr.open('POST', '/sendmail.php', true);
			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об успешной отправке письма
            
					} else {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об ошибке
           
					}
				} else {
					// здесь расположен код вашей callback-функции
					// например, она может выводить сообщение об ошибке
				}
			}

			// отправляем данные формы
			xhr.send(formData);
			formButton.addEventListener('click', () => {
                form.classList.toggle('form--hide');
                formSent.classList.toggle('sent__form--show');
            })
		};
	}

	
	const forms = document.querySelectorAll('[name=feedback]');
	if (!forms) return;
	for (let form of forms) {
		const f = new Form(form);
	}
})();


// Табы //
const tabs = document.querySelector(".tabs__wrapper");
const tabButton = document.querySelectorAll(".tab__button");
const contents = document.querySelectorAll(".tab__content");

tabs.onclick = e => {
  const id = e.target.dataset.id;
  if (id) {
    tabButton.forEach(btn => {
      btn.classList.remove("tab__active");
    });
    e.target.classList.add("tab__active");

    contents.forEach(content => {
      content.classList.remove("tab__active");
    });
    const element = document.getElementById(id);
    element.classList.add("tab__active");
  }
}

// Слайдер //
$(".navigation-slick-carousel").slick({
	infinite: true,
	slidesToShow: 5, 
	slidesToScroll: 1, 
	arrows: false, 
	responsive: [
		{
			breakpoint: 1230,
			settings: {
				slidesToShow: 4,
				variableWidth: true,
			}
		},

		{
			breakpoint: 1030,
			settings: {
				slidesToShow: 3,
				centerMode: true,
				variableWidth: true,
			}
		},

		{
			breakpoint: 880,
			settings: {
				slidesToShow: 2,
				variableWidth: true,
				centerMode: true,
			}
		},

		{
			breakpoint: 630,
			settings: {
				slidesToShow: 2,
				variableWidth: true,
				centerMode: false,
			}
		},

		{
			breakpoint: 510,
			settings: {
				slidesToShow: 1,
				variableWidth: true,
				centerMode: true,
			}
		},

		{
			breakpoint: 360,
			settings: {
				slidesToShow: 1,
				variableWidth: true,
				centerMode: false,
			}
		},
	]
});

$(".sales-slick-carousel").slick({
	infinite: true,
	slidesToShow: 5, 
	slidesToScroll: 1, 
	arrows: true, 
	prevArrow: $('.prev__sales'),
	nextArrow: $('.next__sales'),
	responsive: [
		{
			breakpoint: 1230,
			settings: {
				slidesToShow: 4,
				variableWidth: true,
			}
		},

		{
			breakpoint: 1030,
			settings: {
				slidesToShow: 3,
				centerMode: true,
				variableWidth: true,
			}
		},

		{
			breakpoint: 880,
			settings: {
				slidesToShow: 2,
				variableWidth: true,
				centerMode: true,
			}
		},

		{
			breakpoint: 630,
			settings: {
				slidesToShow: 2,
				variableWidth: true,
				centerMode: false,
			}
		},

		{
			breakpoint: 510,
			settings: {
				slidesToShow: 1,
				variableWidth: true,
				centerMode: true,
			}
		},

		{
			breakpoint: 360,
			settings: {
				slidesToShow: 1,
				variableWidth: true,
				centerMode: false,
			}
		},
	]
});

$(".reviews-slick-carousel").slick({
	infinite: true,
	slidesToShow: 2, 
	slidesToScroll: 1, 
	arrows: true, 
	prevArrow: $('.prev__reviews'),
	nextArrow: $('.next__reviews'),
	responsive: [
		{
			breakpoint: 1230,
			settings: {
				centerMode: true,
				variableWidth: true,
			}
		},

		{
			breakpoint: 768,
			settings: {
				centerMode: true,
				variableWidth: true,
				slidesToShow: 1, 
			}
		},
	]
	
});

$(".price-slick-carousel").slick({
	infinite: true,
	slidesToShow: 3, 
	slidesToScroll: 1, 
	arrows: true, 
	prevArrow: $('.prev__price'),
	nextArrow: $('.next__price'),
	responsive: [
		{
			breakpoint: 1230,
			settings: {
				centerMode: true,
				variableWidth: true,
			}
		},

		{
			breakpoint: 768,
			settings: {
				centerMode: true,
				variableWidth: true,
				slidesToShow: 1, 
			}
		},
	]

});

// Переходы по якорям //
const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href').substr(1)
    
    document.getElementById(blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}





  